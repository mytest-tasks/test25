@extends('layouts.app')

@section('content')
    <h1><a href="/">Home / </a>{{ $car->mark->name }} {{ $car->carmodel->name }},  {{ $car->issue_year }} г.</h1>
    <div class="row">
        <div class="col-md-6 descr__img" style="background-image: url('{{  url($car->image) }}');"></div>
        <div class="col-md-6 descr__text" >
            <ul class="descr__list-item">
                <li class="descr__list-item">Пробег: {{ $car->milage }} км</li>
                <li class="descr__list-item">Цвет: {{ $car->color }}</li>
                <li class="descr__list-item">Стоимость: {{ $car->price }} р.</li>
            </ul>
        </div>
    </div>


@endsection
