@extends('layouts.app')

@section('content')
  <h1>Автомобили</h1>
  @if(!$cars->count())
    <div class="alert alert-danger">Список пуст!</div>
  @else
  <div class="row">
    @foreach($cars as $car)
      <a class="col-6 col-md-4 col-lg-3 card" href="/cars/{{ $car->id }}">
        <div class="card__logo" style="background-image: url('{{  url($car->image) }}');"></div>
        <div class="card__title" >{{ $car->mark->name }} {{ $car->carmodel->name }}</div>
      </a>
    @endforeach
  </div>
  @endif

@endsection
