<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<!-- <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li> -->
<li><a href="{{ backpack_url('elfinder') }}"><i class="fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>

<li class="treeview">
    <a href="#"><i class="fa fa-book"></i> <span>Справочники</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('mark') }}"><i class='fa fa-bus'></i> <span>Марки</span></a></li>
        <li><a href="{{ backpack_url('carmodel') }}"><i class='fa fa-tag'></i> <span>Модели</span></a></li>
    </ul>
</li>
<li><a href="{{ backpack_url('car') }}"><i class='fa fa-car'></i> <span>Автомобили</span></a></li>
