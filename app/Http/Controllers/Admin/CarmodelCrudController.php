<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CarmodelRequest as StoreRequest;
use App\Http\Requests\CarmodelRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CarmodelCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarmodelCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Carmodel');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/carmodel');
        $this->crud->setEntityNameStrings('Модель', 'Модели');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();


        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Модель'
        ]);

        $this->crud->addColumn([
            'name' => 'mark_id', // the column that contains the ID of that connected entity;
            'label' => "Марка", // Table column heading
            'type' => "select",
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Mark",
        ]);

        // -------------------------

        $this->crud->addField([
            'label' => "Выберите марку",
            'type' => 'select2',
            'name' => 'mark_id', // the db column for the foreign key
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Mark" // foreign key model
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Модель автомобиля',
            'type' => 'text',
        ]);

        // add asterisk for fields that are required in CarmodelRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
