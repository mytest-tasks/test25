<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CarRequest as StoreRequest;
use App\Http\Requests\CarRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CarCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CarCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Car');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/car');
        $this->crud->setEntityNameStrings('Автомобиль', 'Автомобили');
        $this->crud->orderBy('lft');
        $this->crud->allowAccess('reorder');
        $this->crud->enableReorder('car_name', 1);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();


        $this->crud->addColumn([
            'name' => 'mark_id', // the column that contains the ID of that connected entity;
            'label' => "Марка", // Table column heading
            'type' => "select",
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Mark",
        ]);

        $this->crud->addColumn([
            'name' => 'carmodel_id', // the column that contains the ID of that connected entity;
            'label' => "Модель", // Table column heading
            'type' => "select",
            'entity' => 'carmodel', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Carmodel",
        ]);

        $this->crud->addColumn([
            'name' => 'image', // The db column name
            'label' => "Изображение", // Table column heading
            'type' => 'image',
             // 'prefix' => 'folder/subfolder/',
             // optional width/height if 25px is not ok with you
             // 'height' => '30px',
             // 'width' => '30px',
         ]);

        $this->crud->addColumn([
            'name' => 'issue_year',
            'type' => 'number',
            'label' => 'Год выпуска'
        ]);

        $this->crud->addColumn([
            'name' => 'milage',
            'type' => 'text',
            'label' => 'Пробег'
        ]);

        $this->crud->addColumn([
            'name' => 'color',
            'type' => 'text',
            'label' => 'Цвет'
        ]);

        $this->crud->addColumn([
            'name' => 'price',
            'type' => 'text',
            'label' => 'Стоимость'
        ]);


        // -------------------------

        $this->crud->addField([
            'label' => "Выберите марку",
            'type' => 'select2',
            'name' => 'mark_id', // the db column for the foreign key
            'entity' => 'mark', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Mark" // foreign key model
        ]);

        $this->crud->addField([
            'label' => "Выберите модель",
            'type' => 'select2',
            'name' => 'carmodel_id', // the db column for the foreign key
            'entity' => 'carmodel', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Carmodel" // foreign key model
        ]);

        $this->crud->addField([ // image
            'label' => "Image",
            'name' => "image",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 4/3, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            'prefix' => '' // in case you only store the filename in the database, this text will be prepended to the database value
        ]);

        $this->crud->addField([
            'name' => 'issue_year',
            'label' => 'Год выпуска',
            'type' => 'number',
        ]);

        $this->crud->addField([
            'name' => 'milage',
            'label' => 'Пробег',
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'color',
            'label' => 'Цвет',
            'type' => 'text',
        ]);

        $this->crud->addField([
            'name' => 'price',
            'label' => 'Стоимость',
            'type' => 'text',
        ]);

        // -------------------------

        // $this->crud->addFilter([ // dropdown filter
        //     'name' => 'select_from_array',
        //     'type' => 'dropdown',
        //     'label'=> 'Dropdown',
        // ], ['one' => 'One', 'two' => 'Two', 'three' => 'Three'], function ($value) {
        //     // if the filter is active
        //     $this->crud->addClause('where', 'select_from_array', $value);
        // });

        $this->crud->addFilter([ // dropdown filter
            'name' => 'select_from_array',
            'type' => 'dropdown',
            'label'=> 'Dropdown',
        ], function () {
            return \App\Models\Mark::all()->keyBy('id')->pluck('name', 'id')->toArray();
        }, function ($value) {
            // if the filter is active
            $this->crud->addClause('where', 'mark_id', $value);
        });

        // add asterisk for fields that are required in CarRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
