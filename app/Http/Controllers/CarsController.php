<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Car;

class CarsController extends Controller
{
    public function index() {
        $cars = Car::orderBy('lft')->get();

        return view('cars.index', compact('cars'));
    }

    public function show(Car $car) {
        //return $car;
        return view('cars.show', compact('car'));
    }


}
