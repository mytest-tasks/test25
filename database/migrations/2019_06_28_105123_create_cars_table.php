<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mark_id');
            $table->unsignedInteger('carmodel_id');
            $table->string('image');
            $table->integer('issue_year');
            $table->string('milage');
            $table->string('color');
            $table->string('price');
            $table->timestamps();

            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('lft')->unsigned()->nullable();
            $table->integer('rgt')->unsigned()->nullable();
            $table->integer('depth')->unsigned()->nullable();

            $table->foreign('mark_id')->references('id')->on('marks')->onDelete('cascade');
            $table->foreign('carmodel_id')->references('id')->on('carmodels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
